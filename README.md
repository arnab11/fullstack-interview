# fullstack-interview



## Project instructions
Your task is to create a simple phone book application with CRUD operations. Use Typescript, React, React context, react-query, material UI. For the back-end, use Typescript, Apollo GraphQL and any database of your choice. Use docker compose to deploy the entire stack



Your application should provide a form where you can enter in first name, last name, and phone number. You should then be able to perform 4 basic operations: create new entries in your database, read the entries, update entries by editing any of the properties, and delete the entries. It should look something like the image below.

![image.png](./image.png)

## How to submit
Upload your completed project to your GitHub/Gitlab, and share the link with the recruiter along with any comments you have about your solution. Please include instructions on how to run the application in your README.md.
